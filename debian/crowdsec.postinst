#!/bin/sh
set -e

# See README.Debian for the distinction between online and offline
# hubs:
OFFLINE_HUB=/usr/share/crowdsec/hub
LIVE_HUB=/var/lib/crowdsec/hub
ITEMS="blockers collections parsers postoverflows scenarios .index.json"

# Offline hub = symlinks are in place, so that an updated Debian
# package ships updated items from the hub:
disable_online_hub() {
  rm -rf "$LIVE_HUB"
  mkdir "$LIVE_HUB"
  for item in $ITEMS; do
    ln -s "$OFFLINE_HUB/$item" "$LIVE_HUB"
  done
}

# Online hub = we replace symlinks with a copy of the items they point
# to, so that enabled items (symlinks from /etc) aren't disabled
# because of dangling symlinks. Let `cscli hub upgrade` replace the
# original copy as required:
enable_online_hub() {
  # Idempotence: once this function has been called once, .index.json
  # should no longer be a symlink, so it can be called each time
  # `cscli hub update` is called:
  if [ -L "$LIVE_HUB/.index.json" ]; then
    echo "I: Switching from offline hub to online hub (see README.Debian)"
    for item in $ITEMS; do
      if [ -L "$LIVE_HUB/$item" ]; then
        rm -f "$LIVE_HUB/$item"
        cp -r "$OFFLINE_HUB/$item" "$LIVE_HUB"
      fi
    done
  fi
}


CAPI=/etc/crowdsec/online_api_credentials.yaml
LAPI=/etc/crowdsec/local_api_credentials.yaml

if [ "$1" = configure ]; then
  if [ ! -f "$LAPI" ]; then
    echo "I: Registering to LAPI ($LAPI)"
    touch "$LAPI"
    # This is required as of 1.0.8 at least:
    touch "$CAPI"

    # Minimal environments (e.g. piuparts):
    if [ ! -f /etc/machine-id ]; then
      echo "W: Missing /etc/machine-id, initializing"
      sed 's/-//g' < /proc/sys/kernel/random/uuid > /etc/machine-id
    fi

    cscli machines add --force "$(cat /etc/machine-id)" --password "$(tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 32 | head -n 1)"
  fi

  # Heuristics: if the file is empty, it's probably been just created
  # by the touch call above, and we want to register. Otherwise,
  # either the user has created a file in advance to disable CAPI
  # registration, or we've already registered to CAPI in a previous
  # configure run (in both cases, don't do anything):
  if [ ! -s "$CAPI" ]; then
    echo "I: Registering to CAPI ($CAPI)"
    cscli capi register
  fi

  # Missing index means initial install, let's go for setting up
  # offline hub + enabling everything per upstream recommendation:
  if [ ! -e /var/lib/crowdsec/hub/.index.json ]; then
    echo "I: Setting up offline hub (see README.Debian)"
    disable_online_hub

    # Symlinks:
    echo "I: Enabling all items (via symlinks from /etc/crowdsec)"
    find /var/lib/crowdsec/hub/*/ -name '*yaml' | \
      while read target; do
        source=${target##/var/lib/crowdsec/hub/}
        # Code as of 1.0.8 is picky about the number of
        # (sub)directories, so the vendor must be stripped:
        source=$(dirname $(dirname "$source"))/$(basename "$source")
        mkdir -p /etc/crowdsec/$(dirname "$source")
        ln -s "$target" "/etc/crowdsec/$source"
      done

    # Initial copy of data files:
    cp /usr/share/crowdsec/data/* /var/lib/crowdsec/data/
  fi
fi

case "$1" in
  disable-online-hub)
    disable_online_hub
    echo "I: Don't forget to inspect the config, and run 'systemctl restart crowdsec' afterward"
    ;;
  enable-online-hub)
    enable_online_hub
    ;;
esac


#DEBHELPER#
